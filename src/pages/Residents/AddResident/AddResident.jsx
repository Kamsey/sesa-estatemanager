import React from 'react'
import { Button } from 'react-bootstrap'
import {Link} from 'react-router-dom'
import ResidentForm from '../Form/Form'

function AddResident() {

    const residentDetails = {
    f_name: "",
    m_name: "",
    l_name: "",
    dob: "",
    email: "",
    phone: "",
    image: [],
    gender: "Male",
    id_type: "Passport",
    id_number: "",
    //estate_id:currentUser.estates[0].id
    estate_id: 1,
  }
  
  return (
    <div>
      <div className='justify-between flex'>
        <div>
        <Link to='/residents' className='no-underline text-sm'>
            Residents
        </Link> <span className='text-sm'>/ Add Resident</span> 
        </div>
        <Button size="sm">Bulk upload</Button>
      </div>
          
        <div className="bg-white p-4 mt-9 rounded">
            <ResidentForm residentDetails={residentDetails}/>
        </div>
    
    </div>
  
  )
}

export default AddResident