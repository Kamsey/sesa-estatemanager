import React, { useContext, useState } from "react";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import SuccessModal from "../../../components/SuccessMessage/SuccessMessage";
import { Upload } from "../../../components/Upload/Upload";
import UploadFileImage from "../../../images/photo_library.svg";
import { Box, FormControl, MenuItem, Select } from "@mui/material";
import User from "../../../components/store/auth";

function ResidentForm(props) {

  const {residentDetails} = props
  const [modalShow, setModalShow] = useState(false);

  //eslint-disable-next-line
  const { currentUser } = useContext(User);

  const getImage = (image) => {
    console.log(image);
  };

  const [residentData, setResidentData] = useState(residentDetails);

  console.log(residentData);

  const handleChange = (e) => {
    setResidentData((prev) => {
      return { ...prev, [e.target.name]: e.target.value };
    });
  };

  const handleSuccessMessage = async (e) => {
    e.preventDefault();
    //     const token = localStorage.getItem("sesaToken")
    //      const req = await fetch('https://real.sesadigital.com/api/createUser',{
    //     method:"POST",
    //     headers:{
    //       "Accept":"application/json",
    //         "Content-Type": "application/json",
    //        "Authorization": `Bearer ${token}`
    //     },
    //     body: JSON.stringify(residentDetails)
    //   })

    //   const data = await req.json()

    //     console.log(data);
    setModalShow(true);
  };

  return (
    <Form onSubmit={handleSuccessMessage} className="mt-8">
      <div className="row mb-8">
        <div className="mb-10">
          <p className="m-0 text-lg">Resident Information</p>
        </div>

        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">
            First Name <span className="text-danger">*</span>
          </label>
          <Form.Control name="f_name" value={residentData.f_name} onChange={handleChange} />
        </div>
        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">Middle Name</label>
          <Form.Control name="m_name" value={residentData.m_name} onChange={handleChange} />
        </div>
        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">
            Last Name <span className="text-danger">*</span>
          </label>
          <Form.Control name="l_name" value={residentData.l_name} onChange={handleChange} />
        </div>
        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">
            Date Of Birth <span className="text-danger">*</span>
          </label>
          <Form.Control name="dob" onChange={handleChange} value={residentData.dob} type="date" />
        </div>
        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">
            Phone Number <span className="text-danger">*</span>
          </label>
          <div className="flex items-center justify-between">
            <span>
              <select name="" id="" className="border p-2 rounded">
                <option value="">+234</option>
              </select>
            </span>
            <Form.Control
              type="number"
              name="phone"
              onChange={handleChange}
              style={{ width: "84%" }}
              value={residentData.phone}
            />
          </div>
        </div>
        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">
            Email Address <span className="text-danger">*</span>
          </label>
          <Form.Control type="email" name="email" value={residentData.email} onChange={handleChange} />
        </div>

        <div className="col-lg-6 col-md-12 mb-5">
          <label className="text-sm mb-2">
            Gender<span className="text-danger">*</span>
          </label>
          <Box sx={{ minWidth: 120 }}>
            <FormControl fullWidth size="small">
              <Select
                sx={{
                  "& legend": { display: "none" },
                  "& fieldset": { top: 0 },
                }}
                name="gender"
                onChange={handleChange}
                value={residentData.gender}
              >
                <MenuItem value={"Male"}>Male</MenuItem>
                <MenuItem value={"Female"}>Female</MenuItem>
              </Select>
            </FormControl>
          </Box>
        </div>

          <div className="col-12 mb-10">
            <Upload
              getImage={getImage}
              bodyText={
                <div className="text-center py-3 fileUploadContainer">
                  <span className="flex justify-center items-center">
                    <img src={UploadFileImage} alt="Upload file" />
                    <p className="mb-0">
                      Drag Resident picture here or{" "}
                      <span className="text-primary pointer">click</span> to
                      upload
                    </p>
                  </span>
                </div>
              }
            />
          </div>

        <h4 className="mb-8">
          KYR <span className="font-light">(Know Your Resident)</span>{" "}
        </h4>
        <div className="row">
          <div className="col-lg-6 mb-5">
            <label className="text-sm mb-2">ID Type</label>
            <Box sx={{ minWidth: 120 }}>
              <FormControl fullWidth size="small">
                <Select
                  sx={{
                    "& legend": { display: "none" },
                    "& fieldset": { top: 0 },
                  }}
                  name="id_type"
                  onChange={handleChange}
                  value={residentData.id_type}
                >
                  <MenuItem value={"Passport"}>Passport</MenuItem>
                  <MenuItem value={"Phone Number"}>Phone Number</MenuItem>
                </Select>
              </FormControl>
            </Box>
          </div>
          <div className="col-lg-6 mb-5">
            <label className="text-sm mb-2">ID Number</label>
            <Form.Control name="id_number" value={residentData.id_number} onChange={handleChange} />
          </div>
            <div className="col-12">
              <Link className="no-underline text-sm" to={"/kyr"}>
                What is KYR?
              </Link>
            </div>
        </div>


          <div className="col-12 mt-16">
            <button className="btn btn-primary float-right">
              Add Resident
            </button>
          </div>

      </div>

      <SuccessModal
        text="a Resident"
        redirecturl="/residents/resident"
        id="1"
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </Form>
  );
}

export default ResidentForm;
