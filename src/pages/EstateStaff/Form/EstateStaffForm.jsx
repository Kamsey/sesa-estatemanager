import React, { useState } from "react";
import { Form } from "react-bootstrap";
import {  Link } from "react-router-dom";
import SuccessModal from "../../../components/SuccessMessage/SuccessMessage";
import CSS from "../../Household/Household.module.css";
import UploadFileImage from "../../../images/photo_library.svg";
import { Upload } from "../../../components/Upload/Upload";
import { Box, FormControl, MenuItem, Select } from "@mui/material";
import MultipleSelectChip from "../../../components/MultiSelect/MultipleSelect";

function EstateStaffForm() {
  const [modalShow, setModalShow] = useState(false);


  const handleSuccessMessage = (e) => {
    e.preventDefault();
    setModalShow(true);
  };

  const getWeekdays = (weekdays) => {
    console.log(weekdays); // get weekdays from multiple select tab
  };

  const getImage = (image) => {
    console.log(image);
  };

  return (
    <Form onSubmit={handleSuccessMessage} className="mt-8">

      <div className="row mb-8">
        <p className="text-lg mb-10">Estate Staff Information</p>
        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">
            First Name <span className="text-danger">*</span>
          </label>
          <Form.Control />
        </div>
        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">
            Last Name <span className="text-danger">*</span>
          </label>
          <Form.Control />
        </div>
        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">Middle Name</label>
          <Form.Control />
        </div>

        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">
            Email Address <span className="text-danger">*</span>
          </label>
          <Form.Control type="email" />
        </div>

        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">
            Phone Number <span className="text-danger">*</span>
          </label>
          <div className="flex items-center justify-between">
            <span>
              <select name="" id="" className="border p-2 rounded">
                <option value="">+234</option>
              </select>
            </span>
            <Form.Control type="number" style={{ width: "84%" }} />
          </div>
        </div>
        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">
            Date Of Birth <span className="text-danger">*</span>
          </label>
          <Form.Control type="date" />
        </div>

        <div className="col-lg-6 col-md-12 mb-3">
          <label className="text-sm mb-2">
            Gender<span className="text-danger">*</span>
          </label>
          <Box sx={{ minWidth: 120 }}>
            <FormControl fullWidth size="small">
              <Select
                sx={{
                  "& legend": { display: "none" },
                  "& fieldset": { top: 0 },
                }}
                name="gender"
                value={"Male"}
              >
                <MenuItem value={"Male"}>Male</MenuItem>
                <MenuItem value={"Female"}>Female</MenuItem>
              </Select>
            </FormControl>
          </Box>
        </div>

        <div className="col-lg-6 mb-3">
          <label className="text-sm mb-2">
            Home Address <span className="text-danger">*</span>
          </label>
          <Form.Control />
        </div>

        <div className="col-lg-12">
          <div className="col-lg-6 col-md-12 mb-5">
            <MultipleSelectChip getWeekdays={getWeekdays} />
          </div>
        </div>

        <div className="col-lg-12 mb-10">
          <label className="text-sm mb-2">
            Message <span className="text-danger">*</span>
          </label>
          <Form.Control
            required
            as="textarea"
            placeholder="This message will be displayed to the security guard when the estate staff checks in / out"
            rows={6}
            maxLength={30}
          />
          <span className="text-muted text-sm">Maximum of 30 Characters </span>
        </div>

          <div className="col-12 mb-10">
            <div className={`${CSS.imageEstateStaff} mx-auto p-1`}>
              <Upload
                getImage={getImage}
                bodyText={
                  <div className="text-center py-3 fileUploadContainer">
                    <span className="flex justify-center items-center p-2">
                      <img src={UploadFileImage} alt="Upload file" />
                      <p className="mb-0">
                        Drag Estate Staff picture here or{" "}
                        <span className="text-primary pointer">click</span> to
                        upload
                      </p>
                    </span>
                  </div>
                }
              />
            </div>
          </div>

        <h4 className="mb-8">
          KYR <span className="font-light">(Know Your Resident)</span>{" "}
        </h4>

        <div className="row mb-44">
          <div className="col-lg-6 mb-5">
            <label className="text-sm mb-2">ID Type</label>
            <Box sx={{ minWidth: 120 }}>
              <FormControl fullWidth size="small">
                <Select
                  sx={{
                    "& legend": { display: "none" },
                    "& fieldset": { top: 0 },
                  }}
                  name="id_type"
                  value={"Passport"}
                >
                  <MenuItem value={"Passport"}>Passport</MenuItem>
                  <MenuItem value={"Phone Number"}>Phone Number</MenuItem>
                </Select>
              </FormControl>
            </Box>
          </div>
          <div className="col-lg-6 mb-5">
            <label className="text-sm mb-2">ID Number</label>
            <Form.Control />
          </div>
          <div className="col-12">
            <Link className="no-underline text-sm" to={"/kyr"}>
              What is KYR?
            </Link>
          </div>
        </div>

          <div className="col-12 mt-16">
            <button className="btn btn-primary float-right">Add Staff</button>
          </div>

      </div>

      <SuccessModal
        text="an Estate Staff"
        redirecturl="/estate-staff"
        id="1"
        show={modalShow}
        onHide={() => setModalShow(false)}
      />
    </Form>
  );
}

export default EstateStaffForm;
