import React from "react";
import { Link } from "react-router-dom";
import House from "../../../images/House.jpeg";
import { Box, Button, FormControl, MenuItem, Select } from "@mui/material";
import { useState } from "react";
import { Form } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { DeleteButtonTrashIcon } from "../../../components/SideBar/icons";

function EditProperty() {
  const location = useNavigate();

  const [type, setType] = useState("0");
  const [name, setName] = useState("");

  const handlePropertyType = (e) => {
    setType(e.target.value);
    setName("");
  };

  const handleSuccessMessage = (e) => {
    e.preventDefault();
    location("/properties/property/2");
  };

  return (
    <div>
      <Link to="/properties" className="no-underline text-sm">
        Property
      </Link>{" "}
      <span className="text-sm">/ Edit Property</span>
      <div className="bg-white p-4 mt-9 rounded">
        <div className="mt-16">
          <img
            src={House}
            className="w-36 h-36 rounded-full object-cover"
            alt="something"
          />
        </div>
        <div className="mb-20 mt-7">
          <p className="text-muted text-sm">
            Property Code: <span className="text-gray-800">73737</span>{" "}
          </p>
        </div>

        <Form onSubmit={handleSuccessMessage} className="mt-8">
          <div className="row mb-8">
            <div className="mb-10">
              <p className="m-0 text-lg">Property Information</p>
            </div>
            <div className="col-lg-6 mb-3">
              <label className="text-sm mb-2 font-medium">
                Estate <span className="text-danger">*</span>
              </label>
              <Form.Control />
            </div>
            <div className="col-lg-6 mb-3">
              <label className="text-sm mb-2 font-medium">
                Property (Block No. & Flat No)
              </label>
              <Form.Control />
            </div>
            <div className="col-lg-6 mb-3">
              <label className="text-sm mb-2 font-medium">
                Area / Street <span className="text-danger">*</span>
              </label>
              <Form.Control />
            </div>
            <div className="col-lg-6 mb-3">
              <label className="text-sm mb-2 font-medium">
                Property Category <span className="text-danger">*</span>
              </label>
              <Box sx={{ minWidth: 120 }}>
                <FormControl fullWidth size="small">
                  <Select
                    sx={{
                      "& legend": { display: "none" },
                      "& fieldset": { top: 0 },
                    }}
                    name="propertyCategory"
                    value={"1 bed room self con"}
                  >
                    <MenuItem value={"1 bed room self con"}>
                      1 bed room self con
                    </MenuItem>
                    <MenuItem value={"2 bed room self con"}>
                      2 bed room self con
                    </MenuItem>
                  </Select>
                </FormControl>
              </Box>
            </div>
            <div className="col-lg-6 mb-3">
              <label className="text-sm mb-2 font-medium">
                Property Type <span className="text-danger">*</span>
              </label>
              <Box sx={{ minWidth: 120 }}>
                <FormControl fullWidth size="small">
                  <Select
                    sx={{
                      "& legend": { display: "none" },
                      "& fieldset": { top: 0 },
                    }}
                    value={type}
                    onChange={handlePropertyType}
                    name="propertyType"
                  >
                    <MenuItem value={"0"}>Residential</MenuItem>
                    <MenuItem value={"1"}>Business</MenuItem>
                  </Select>
                </FormControl>
              </Box>
            </div>
            <div className="col-lg-6 mb-3">
              <label
                className={`${
                  type === "0" && "text-muted"
                } text-sm mb-2 font-medium`}
              >
                Name <span className="text-danger">*</span>
              </label>
              <Form.Control
                value={name}
                onChange={(e) => setName(e.target.value)}
                disabled={type === "1" ? false : true}
                readOnly={type === "1" ? false : true}
              />
            </div>

            <div className={`col-lg-12 mb-3`}>
              <label className="text-sm mb-2 font-medium">
                Address Description <span className="text-danger">*</span>
              </label>
              <Form.Control
                as={"textarea"}
                rows={6}
                maxLength={80}
                placeholder="This is the address direction that would be displayed on the print out handed to the visitor at check in."
              />
              <p className="mb-0 text-xs text-muted mt-2">
                Maximum of 80 characters
              </p>
            </div>

            <div className="col-lg-12 mt-16">
              <div className="flex justify-between">
                <Button
                  style={{ textTransform: "capitalize" }}
                  variant="outlined"
                  size="small"
                  color="error"
                  startIcon={<DeleteButtonTrashIcon />}
                  type="button"
                >
                  <span className="mr-0">Delete</span>
                </Button>

                <div>
                  <button type="submit" className="btn btn-primary">
                    Save Changes
                  </button>
                </div>
              </div>
            </div>
          </div>
        </Form>
      </div>
    </div>
  );
}

export default EditProperty;
