import React from "react";
import { Form } from "react-bootstrap";
import { useNavigate, Link } from "react-router-dom";
import MultipleSelectChip from "../../../components/MultiSelect/MultipleSelect";
import ProfilePicture from "../../../images/security.jpeg";
import { Box, Button, FormControl, MenuItem, Select } from "@mui/material";
import { DeleteButtonTrashIcon } from "../../../components/SideBar/icons";

function EditSiteWorker() {
  const location = useNavigate();

  const handleSuccessMessage = (e) => {
    e.preventDefault();
    location("/site-worker/2");
  };

  const getWeekdays = (weekdays) => {
    console.log(weekdays); // get weekdays from multiple select tab
  };
  return (
    <div>
      <Link to="/site-worker" className="no-underline text-sm">
        Site Worker
      </Link>{" "}
      <span className="text-sm">/ Edit Site Worker</span>
      <div className="bg-white p-4 mt-8 rounded">
        <div className="mt-10">
          <div className="row">
            <div className="col-lg-2 mb-3">
              <img
                src={ProfilePicture}
                className="w-32 h-32 object-cover rounded-full"
                style={{ border: "3px solid #20B16A" }}
                alt=""
              />
              <Button variant="text" component="label" sx={{ marginLeft: 4 }}>
                Edit
                <input hidden accept="image/*" type="file" />
              </Button>
            </div>
            <div className="col-lg-4 my-auto">
              <p className="text-muted text-sm text-left">
                Staff Code:{" "}
                <span className="font-medium text-black">09873</span>
              </p>
              <p className="text-muted text-sm text-left">
                Work Location:{" "}
                <span className="font-medium text-black">H77363</span>
              </p>
              <p className="text-muted text-sm text-left">
                KYC: <span className="font-medium text-primary">Validated</span>
              </p>
            </div>
          </div>

          <Form className="mt-20 mb-16" onSubmit={handleSuccessMessage}>
            <p className="text-lg mb-10">Site Worker Information</p>
            <div className="row">
              <div className="col-lg-6 mb-3">
                <label className="mb-3">First Name</label>
                <Form.Control />
              </div>

              <div className="col-lg-6 mb-3">
                <label className="mb-3">Last Name</label>
                <Form.Control />
              </div>

              <div className="col-lg-6 mb-3">
                <label className="mb-3">Midlle Name</label>
                <Form.Control />
              </div>

              <div className="col-lg-6 mb-3">
                <label className="mb-3">Home Address</label>
                <Form.Control />
              </div>

              <div className="col-lg-6 mb-3">
                <label className="text-sm mb-3">
                  Phone Number <span className="text-danger">*</span>
                </label>
                <div className="flex items-baseline justify-between">
                  <span>
                    <select name="" id="" className="border p-2 rounded">
                      <option value="">+234</option>
                    </select>
                  </span>
                  <Form.Control
                    type="number"
                    placeholder="Phone Number"
                    style={{ width: "80%" }}
                  />
                </div>
              </div>

              <div className="col-lg-6 mb-3">
                <label className="mb-3">Date Of Birth</label>
                <Form.Control type="date" />
              </div>

              <div className="col-lg-6 col-md-12 mb-3">
                <label className="text-sm mb-3">
                  Gender<span className="text-danger">*</span>
                </label>
                <Box sx={{ minWidth: 120 }}>
                  <FormControl fullWidth size="small">
                    <Select
                      sx={{
                        "& legend": { display: "none" },
                        "& fieldset": { top: 0 },
                      }}
                      name="gender"
                      value={"Male"}
                    >
                      <MenuItem value={"Male"}>Male</MenuItem>
                      <MenuItem value={"Female"}>Female</MenuItem>
                    </Select>
                  </FormControl>
                </Box>
              </div>

              <div className="col-lg-6 mb-3">
                <label className="mb-3">Email Address</label>
                <Form.Control />
              </div>

              <div className="col-lg-6 mb-3">
                <label className="mb-3"></label>
                <MultipleSelectChip getWeekdays={getWeekdays} />
              </div>

              <div className="row">
                <div className="col-lg-6 mb-3">
                  <label className="mb-3">Clock-in-Time</label>
                  <Form.Control type="time" />
                  <span className="text-muted text-sm">
                    Enforcing coming in
                  </span>
                </div>

                <div className="col-lg-6 mb-3">
                  <label className="mb-3">Clock-out-Time</label>
                  <Form.Control type="time" />
                </div>
              </div>

              <div className="col-lg-12 mb-8">
                <label className="mb-3">Security guard Message</label>
                <Form.Control
                  as={"textarea"}
                  placeholder="This message will be displayed to the security guard when the site worker checks in / out"
                  rows={6}
                  maxLength={30}
                />
                <span className="text-muted text-sm">
                  Maximum of 30 Characters{" "}
                </span>
              </div>

              <h4 className="mb-8">
                KYR <span className="font-light">(Know Your Resident)</span>{" "}
              </h4>

              <div className="row mb-44" style={{ marginTop: 30 }}>
                <div className="col-lg-6 mb-5">
                  <label className="text-sm mb-2">ID Type</label>
                  <Box sx={{ minWidth: 120 }}>
                    <FormControl fullWidth size="small">
                      <Select
                        sx={{
                          "& legend": { display: "none" },
                          "& fieldset": { top: 0 },
                        }}
                        name="id_type"
                        value={"Passport"}
                      >
                        <MenuItem value={"Passport"}>Passport</MenuItem>
                        <MenuItem value={"Phone Number"}>Phone Number</MenuItem>
                      </Select>
                    </FormControl>
                  </Box>
                </div>
                <div className="col-lg-6 mb-5">
                  <label className="text-sm mb-2">ID Number</label>
                  <Form.Control />
                </div>

                <div className="col-12">
                  <Link className="no-underline text-sm" to={"/kyr"}>
                    What is KYR?
                  </Link>
                </div>
              </div>
            </div>

            <div className="flex justify-between items-center">
              <div>
                <button
                  style={{ backgroundColor: "#FF9500" }}
                  className="btn btn-sm my-2 text-white mr-10"
                  type="button"
                >
                  Decativate
                </button>
                <Button
                  style={{ textTransform: "capitalize" }}
                  variant="outlined"
                  size="small"
                  color="error"
                  startIcon={<DeleteButtonTrashIcon />}
                  type="button"
                >
                  <span className="mr-0">Delete</span>
                </Button>
              </div>

              <div>
                <button className="btn btn-primary btn-sm">Save Changes</button>
              </div>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default EditSiteWorker;
